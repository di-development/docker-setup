#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/config.sh"

function usage {
    echo "usage:"
    echo "$0 ENVIRONMENT WEBSERVER DATABASE project:KULT [domain:boilerplate.docker] [db:dbname] [php:7.2]"
    echo
    echo "ENVIRONMENT:"
    echo " development      Development environment"
    echo " production       Production environment"
    echo
    echo "WEBSERVER:"
    echo " apache           Apache 2.4 (HTTPd)"
    echo " nginx            Nginx high performance Webserver"
    echo
    echo "DATABASE:"
    echo " mysql            MySQL 5.7"
    echo " mariadb          MariaDB 10.x"
    echo
    echo "project:          Project name, e.g. project:NADA"
    echo
    echo "domain:           Domain name for environment, e.g. domain:kult.docker"
    echo
    echo "db:               Database name to use, e.g. db:dbname"
    echo
    echo "php:              PHP version to use. Supported are 5.6, 7.0, 7.1, 7.2"
    echo
    echo "You can also choose webserver, database and php version by hand!"
    echo "To do so, in folder 'docker/web' copy either Dockerfile.apache or Dockerfile.nginx to Dockerfile"
    echo "and in folder 'docker/mysql' copy either one of the MySQL-x.x.Dockerfile or MariaDB-x.Dockerfile to Dockerfile"
    echo "To use specific PHP version, edit docker/web/Dockerfile and choose one of the webserver"
}


function parseArg {
    case "$1" in
        ###################################
        ## Show help
        ###################################
        "help")
            usage
            ;;

        ###################################
        ## Webserver
        ###################################
        "apache")
            logMsg "Webserver: Apache"
            _WEBSERVER="apache"
            ;;
        "nginx")
            logMsg "Webserver: Nginx"
            _WEBSERVER="nginx"
            ;;

        ###################################
        ## Database
        ###################################
        "mysql")
            logMsg "Database: MySQL"
            cp -af "$(pwd)/docker/mysql/MySQL-5.7.Dockerfile" "$(pwd)/docker/mysql/Dockerfile"
            _DATABASE="mysql"
            ;;

        "mariadb")
            logMsg "Database: MariaDB"
            cp -af "$(pwd)/docker/mysql/MariaDB-10.Dockerfile" "$(pwd)/docker/mysql/Dockerfile"
            _DATABASE="mariadb"
            ;;

        ###################################
        ## Environment
        ###################################
        "development")
            logMsg "Environment: development"
            cp -af "$(pwd)/docker-compose.development.yml" "$(pwd)/docker-compose.yml"
            _ENV="development"
            ;;
        "production")
            logMsg "Environment: production"
            cp -af "$(pwd)/docker-compose.production.yml" "$(pwd)/docker-compose.yml"
            _ENV="production"
            ;;


        domain*)
            _DOMAIN=${1//domain:/}
            logMsg "Domain: $_DOMAIN"

            logMsg "Patching docker-compose.yml"
            sed -i '' -E "s/\.boilerplate\.docker/.$_DOMAIN/g" "$(pwd)/docker-compose.yml"

            logMsg "Patching vhosts"
            for f in `ls $DOCKER_DIR/etc/*/vhost.d/*.conf 2>/dev/null`
            do
                sed -i '' -E "s/$OLD_DOMAIN/$_DOMAIN/g" $f
            done
            ;;


        db*)
            MYSQL_DATABASE=${1//db:/}
            logMsg "DB: $MYSQL_DATABASE"

            logMsg "Patching environment.yml: MYSQL_DATABASE=$MYSQL_DATABASE"
            sed -i '' -E "s/MYSQL_DATABASE=.*/MYSQL_DATABASE=$MYSQL_DATABASE/" "$(pwd)/docker/web/conf/environment.yml"
            ;;


        project*)
            _PROJECT=`echo ${1//project:/} | tr '[:upper:]' '[:lower:]'`
            logMsg "Project: $_PROJECT"
            ;;


        php*)
            _PHP=${1//php:/}
            
            # PHP Versions
            php_versions="5.6 7.0 7.1 7.2"
            if [[ " $php_versions " =~ .*\ $_PHP\ .* ]]; then
                logMsg "PHP: $_PHP"
            else
                errorMsg "Invalid PHP version $_PHP! Supported versions are 5.6, 7.0, 7.1, 7.2"
                _PHP=""
            fi
            ;;


        ###################################
        ## Catch invalid option
        ###################################
        *)
            logMsg "Invalid argument: $1"
            exit 1
        ;;
    esac
}


# Environement
_MYIP=""
_ENV=""
_WEBSERVER=""
_DATABASE=""
_PROJECT=""
_PHP="7.2"
MYSQL_DATABASE="dbname"
_DOMAIN="boilerplate.docker"
OLD_DOMAIN=$_DOMAIN
OLD_DBNAME=$MYSQL_DATABASE
_NETWORK_NAME="kult-dev-network"

unamestr=`uname`

# Read in current.conf
if [ -f "$(pwd)/current.conf" ]; then
    source "$(pwd)/current.conf"
    if [ $_DOMAIN != "" ]; then
        OLD_DOMAIN=$_DOMAIN
    fi
    if [ $OLD_DBNAME != $MYSQL_DATABASE ]; then
        OLD_DBNAME=$MYSQL_DATABASE
    fi
fi

# Check for single command
if [ "$#" -eq 1 ]; then
    if [ $1 == "show" ]; then
        sectionHeader "Current configuration:"
        logMsg " Project:     $_PROJECT"
        logMsg " Environment: $_ENV"
        logMsg " Webserver:   $_WEBSERVER"
        logMsg " PHP:         $_PHP"
        logMsg " Database:    $_DATABASE"
        logMsg " DB Name:     $MYSQL_DATABASE"
        logMsg " Domain:      $_DOMAIN"
        logMsg " xdebug.remote_host: $_MYIP"
        exit 0
    else
        usage
        exit 0
    fi
fi

# Require all arguments
if [ "$#" -lt 3 ]; then
    echo "No option defined!"
    echo
    usage
    exit 0
fi

# Remove old files
rm -f "$(pwd)/docker-compose.yml"
rm -f "$(pwd)/docker/mysql/Dockerfile"
rm -f "$(pwd)/docker/web/Dockerfile"
rm -f "$(pwd)/etc/*/vhost.d/*.conf"
rm -f "$(pwd)/.env"

# Create main vhost
cp -af "$(pwd)/docker/web/conf/httpd/vhost.d/MAIN.skel" "$(pwd)/etc/httpd/vhost.d/99-main.conf"
cp -af "$(pwd)/docker/web/conf/nginx/vhost.d/MAIN.skel" "$(pwd)/etc/nginx/vhost.d/99-main.conf"

# Check each argument
sectionHeader "Environment configuration"
for arg in $*; do
    parseArg $arg
done

# Require valid ENV
if [ "$_ENV" == "" ]; then
    errorMsg "Environment not defined!"
    exit 1
fi

# Require valid PHP version
# Error message already thrown
if [ "$_PHP" == "" ]; then
    exit 1
fi

# Require valid PHP version
if [ "$_PROJECT" == "" ]; then
    errorMsg "Project not defined!"
    exit 1
fi

# Copy Dockerfile for requested webserver and environment
cp -af "$(pwd)/docker/web/Dockerfile.$_WEBSERVER.$_ENV" "$(pwd)/docker/web/Dockerfile"

# Get IP of developer
if [ $IS_APFEL == 1 ]; then
    IFACE=$(route -n get 0.0.0.0 2>/dev/null | awk '/interface: / {print $2}')
    _MYIP=`ifconfig $IFACE|grep "inet " |cut -d" " -f2`
else
    IFACE=$(route -n 2>/dev/null | grep "^0.0.0.0" | rev | cut -d' ' -f1 | rev)
    _MYIP=`ifconfig $IFACE|grep "inet " |cut -d" " -f10`
fi

# Patch php.ini
logMsg "Patching web/Dockerfile: PHP=$_PHP"
sed -i '' -E "s/FROM webdevops\/php.*/FROM webdevops\/php-$_WEBSERVER-dev:$_PHP/" "$(pwd)/docker/web/Dockerfile"
sed -i '' -E "s/ENV PROVISION_CONTEXT.*/ENV PROVISION_CONTEXT=\"${_ENV}\"/" "$(pwd)/docker/web/Dockerfile"

# Modify docker-compose.yml
logMsg "Patching docker-compose.yml"
_WS=$_WEBSERVER
if [ "$_WS" == 'apache' ]; then
    _WS='httpd'
fi
sed -i '' -E "s/_PROJECT_/${_PROJECT}/g" "$(pwd)/docker-compose.yml"
sed -i '' -E "s/_WEBSERVER/${_WS}/g" "$(pwd)/docker-compose.yml"


# create .env file
cp -a "$(pwd)/env-sample" "$(pwd)/.env"

# Setup project name
logMsg "Patching .env"
sed -i '' -E "s/COMPOSE_PROJECT_NAME=.*$/COMPOSE_PROJECT_NAME=${_PROJECT}/" "$(pwd)/.env"

# Patch php.ini
if [ "$_ENV" == "development" ]; then
    logMsg "Patching php.ini: xdebug.remote_host=$_MYIP"
    sed -i '' -E "s/xdebug\.remote_host.*/xdebug\.remote_host='$_MYIP'/" "$(pwd)/docker/web/etc/php/development.ini"
fi

# Write down env config
echo "" > "$(pwd)/current.conf"
echo "_ENV=\"$_ENV\"" >> "$(pwd)/current.conf"
echo "_WEBSERVER=\"$_WEBSERVER\"" >> "$(pwd)/current.conf"
echo "_DATABASE=\"$_DATABASE\"" >> "$(pwd)/current.conf"
echo "MYSQL_DATABASE=\"$MYSQL_DATABASE\"" >> "$(pwd)/current.conf"
echo "_MYIP=\"$_MYIP\"" >> "$(pwd)/current.conf"
echo "_DOMAIN=\"$_DOMAIN\"" >> "$(pwd)/current.conf"
echo "_PROJECT=\"$_PROJECT\"" >> "$(pwd)/current.conf"
echo "_PHP=\"$_PHP\"" >> "$(pwd)/current.conf"
echo "_NETWORK_NAME=\"${_NETWORK_NAME}\"" >> "$(pwd)/current.conf"
logMsg "Configuration saved to current.conf"


# Create network if missing
if [ "$(docker network ls --filter name="${_NETWORK_NAME}" -q)" == "" ]; then
    # Choose random network
    _OCTET_2=$(( ((RANDOM<<15)|RANDOM) % 254 + 0 ))
    _OCTET_3=$(( ((RANDOM<<15)|RANDOM) % 254 + 0 ))
    logMsg "Create network kult-dev-network 172.${_OCTET_2}.${_OCTET_3}.0/16"
    $(pwd)/bin/network.sh add "${_NETWORK_NAME}" "172.${_OCTET_2}.${_OCTET_3}.0/16"
fi
