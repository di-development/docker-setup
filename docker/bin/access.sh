#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/config.sh"

function usage {
    echo "usage:"
    echo "$0 USER APPLICATION"
    echo
    echo "E.g. make access root app"
}

if [ "$#" -lt 2 ]; then
    usage
    exit
fi

# root/application
USER=$1

# e.g. app
CONTAINER=$2

if [ "$CONTAINER" == "help" ]; then
    usage
    exit 0
fi

logMsg "Access $CONTAINER as $USER"
docker-compose exec --user $USER $CONTAINER /bin/bash

exit 0