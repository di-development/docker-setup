#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/config.sh"

if [ "$#" -lt 1 ]; then
    echo "No project type defined (either git, typo3v7 for latest in version 7, typo3v8 for latest in version 8 or typo3v9 for latest in version 9)"
    exit 1
fi

# If project dir exists backup it with timestamp
TS=$(date +%Y%m%d%H%M%S)
[ ! -d "$PROJECT_DIR" ] || mv "$PROJECT_DIR" "$PROJECT_DIR.$TS";

# Create all required folders
mkdir -p -- "$PROJECT_DIR/src"
mkdir -p -- "$PROJECT_DIR/web_root"
mkdir -p -- "$PROJECT_DIR/doc"
mkdir -p -- "$PROJECT_DIR/db"
chmod -R 770 "$PROJECT_DIR"

# Keep readme.md
if [ -d "$PROJECT_DIR.$TS" ] && [ -f "$PROJECT_DIR.$TS/doc/readme.md" ]; then
    cp -a "$PROJECT_DIR.$TS/doc/readme.md" "$PROJECT_DIR/doc/readme.md"
fi

case "$1" in
    ###################################
    ## TYPO3 CMS
    ###################################
    "typo3v7")
        execInDir "$PROJECT_DIR" "docker run --rm --env COMPOSER_CACHE_DIR=/tmp --user $(id -u):$(id -g) -v $PROJECT_DIR:/project composer create-project typo3/cms-base-distribution src ^7"
        execInDir "$PROJECT_DIR" "touch web/FIRST_INSTALL"
        ;;

    "typo3v8")
        # Checkout typo3 via composer
        execInDir "$PROJECT_DIR" "docker run --rm --env COMPOSER_CACHE_DIR=/tmp --user $(id -u):$(id -g) -v $PROJECT_DIR:/project composer create-project typo3/cms-base-distribution /project/src/typo3v8 ^8"

        # Copy files and create symlinks
        execInDir "$PROJECT_DIR" "ln -sf ./src/typo3v8/vendor ./vendor"
        execInDir "$PROJECT_DIR/web_root" "cp -a ../src/typo3v8/public/index.php ./index.php && \
        ln -sf ../src/typo3v8/public/typo3 ./typo3 && \
        cp -a ../src/typo3v8/public/typo3conf ./typo3conf && \
        cp -a ../src/typo3v8/public/typo3temp ./typo3temp && \
        cp -a ../src/typo3v8/public/fileadmin ./fileadmin && \
        cp -a ../src/typo3v8/public/uploads ./uploads"

        # Create FIRST_INSTALL
        execInDir "$PROJECT_DIR/web_root" "touch FIRST_INSTALL"

        logMsg "Please fix permissions!"
        ;;

    "typo3v9")
        # Checkout typo3 via composer
        execInDir "$PROJECT_DIR" "docker run --rm --env COMPOSER_CACHE_DIR=/tmp --user $(id -u):$(id -g) -v $PROJECT_DIR:/project composer create-project typo3/cms-base-distribution /project/src/typo3v9 ^9"

        # Copy files and create symlinks
        execInDir "$PROJECT_DIR" "ln -sf ./src/typo3v9/vendor ./vendor"
        execInDir "$PROJECT_DIR/web_root" "cp -a ../src/typo3v9/public/index.php ./index.php && \
        ln -sf ../src/typo3v8/public/typo3 ./typo3 && \
        cp -a ../src/typo3v8/public/typo3conf ./typo3conf && \
        cp -a ../src/typo3v8/public/typo3temp ./typo3temp && \
        cp -a ../src/typo3v8/public/fileadmin ./fileadmin && \
        cp -a ../src/typo3v8/public/uploads ./uploads"

        # Create FIRST_INSTALL
        execInDir "$PROJECT_DIR/web_root" "touch FIRST_INSTALL"
        ;;

    ###################################
    ## GIT
    ###################################
    "git")
        if [ "$#" -lt 2 ]; then
            echo "Missing git url"
            exit 1
        fi
        git clone --recursive "$2" "$PROJECT_DIR"

        # Link templating folder
        #[ -d "$PROJECT_DIR/src/templating" ] && ln -sf "$PROJECT_DIR/src/templating/ $PROJECT_DIR/web_root/templating"
        ;;
esac

touch -- "$PROJECT_DIR/.gitkeep"
