#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/config.sh"

function usage {
    echo "usage:"
    echo "$0 COMMAND [ARGS]"
    echo
    echo "Command:"
    echo " show           List all volumes"
    echo " prune          Remove unused volumes"
    echo " rm             Remove volume"
}

if [ "$#" -lt 1 ]; then
    usage
    exit
fi


case "$1" in
    ###################################
    ## Show help
    ###################################
    "help")
        usage
        ;;

    ###################################
    ## List all volumes
    ###################################
    "show")
        sectionHeader "Volumes"
        docker volume ls
        ;;

    ###################################
    ## Remove not used volumes
    ###################################
    "prune")
        sectionHeader "Prune volumes"
        docker volume prune
        logMsg "done"
        ;;

    ###################################
    ## Remove single volume
    ###################################
    "rm")
        if [ "$#" -lt 2 ]; then
            errorMsg "Missing volume"
            exit 1
        fi
        sectionHeader "Remove volume $2"
        docker volume rm $2
        logMsg "done"
        ;;

    ###################################
    ## Catch invalid option
    ###################################
    *)
        if [ -z $1 ]; then
            usage
            exit 0
        else
            errorMsg "Invalid argument: $1"
            exit 1
        fi
    ;;
esac
