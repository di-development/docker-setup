#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/config.sh"

function usage {
    echo "usage:"
    echo "$0 COMMAND VHOST"
    echo
    echo "Command:"
    echo " show           List all vhosts"
    echo " export         Generate line for hosts file"
    echo " add            Add new vhost"
    echo " remove         Remove existing vhost"
    echo
    echo "vhost:          The vhost to add/remove"
}

if [ "$#" -lt 1 ]; then
    usage
    return
fi

# Environement
_WEBSERVER="apache"
_DOMAIN="boilerplate.docker"
OLD_DOMAIN=$_DOMAIN

# Read in current.conf
if [ -f current.conf ]; then
    source current.conf
fi

# Path to vhost files
VHOST_SOURCE_PATH="$(pwd)/docker/web/conf/httpd/vhost.d"
VHOST_PATH="$(pwd)/etc/httpd/vhost.d"
if [ "$_WEBSERVER" == "nginx" ]; then
    VHOST_SOURCE_PATH="$(pwd)/docker/web/conf/nginx/vhost.d"
    VHOST_PATH="$(pwd)/etc/nginx/vhost.d"
fi

case "$1" in
    ###################################
    ## Show help
    ###################################
    "help")
        usage
        ;;

    ###################################
    ## List all vhosts
    ###################################
    "show")
        sectionHeader "Vhosts on domain ${_DOMAIN}"
        logMsg "www -> www.${_DOMAIN}"
        for f in `ls "${VHOST_PATH}/" 2>/dev/null`
        do
            if [ "${f:0:3}" == "10-" ]; then
                CURRENT_SUBDOMAIN=$f
                CURRENT_SUBDOMAIN=${CURRENT_SUBDOMAIN##*10-}
                CURRENT_SUBDOMAIN=${CURRENT_SUBDOMAIN%.*}
                logMsg "${CURRENT_SUBDOMAIN} -> ${CURRENT_SUBDOMAIN}.${_DOMAIN}"
            fi
        done
        ;;

    ###################################
    ## Add new vhost
    ###################################
    "add")
        sectionHeader "Modify vhosts..."
        _SUBDOMAIN=$2

        if [ -f "${VHOST_PATH}/10-${_SUBDOMAIN}.conf" ]; then
            errorMsg "vhost '${_SUBDOMAIN}' on domain ${_DOMAIN} already exists"
        else
            logMsg "Add vhost: $_SUBDOMAIN"
            cp -a "${VHOST_SOURCE_PATH}/SUBDOMAIN.skel" "${VHOST_PATH}/10-${_SUBDOMAIN}.conf"
            if [ `uname` == 'Darwin' ]; then
                sed -i '' "s/\.${OLD_DOMAIN}/.${_DOMAIN}/g" "${VHOST_PATH}/10-${_SUBDOMAIN}.conf"
                sed -i '' "s/SUBDOMAIN/${_SUBDOMAIN}/g" "${VHOST_PATH}/10-${_SUBDOMAIN}.conf"
            else
                sed -i -e  "s/\.${OLD_DOMAIN}/.${_DOMAIN}/g" "${VHOST_PATH}/10-${_SUBDOMAIN}.conf"
                sed -i -e  "s/SUBDOMAIN/${_SUBDOMAIN}/g" "${VHOST_PATH}/10-${_SUBDOMAIN}.conf"
            fi
        fi
        ;;

    ###################################
    ## Remove existing vhost
    ###################################
    "remove")
        sectionHeader "Modify vhosts..."
        if [ "$#" -lt 2 ]; then
            usage
            exit 0
        fi

        _SUBDOMAIN=$2

        logMsg "Remove vhost: $_SUBDOMAIN"
        rm -f "${VHOST_PATH}/10-${_SUBDOMAIN}.conf"
        ;;

    ###################################
    ## Generate line for /etc/hosts
    ###################################
    "export")
        sectionHeader "Please add the following line to your hosts file"

        _LINE=""

        # Find vhosts
        for f in `ls "${VHOST_PATH}/" 2>/dev/null`
        do
            if [ "${f:0:3}" == "10-" ]; then
                CURRENT_SUBDOMAIN=$f
                CURRENT_SUBDOMAIN=${CURRENT_SUBDOMAIN##*10-}
                CURRENT_SUBDOMAIN=${CURRENT_SUBDOMAIN%.*}
                _LINE="$_LINE ${CURRENT_SUBDOMAIN}.${_DOMAIN}"
            fi
        done

        # Find containers
        for c in `docker container ls -f "Name=${_PROJECT}" -q`
        do
            [[ `docker inspect --format "{{ .Config.Env }}" ${c}` =~ VIRTUAL_HOST=([^\.][a-zA-Z0-9\.\-]*) ]] && _LINE="$_LINE ${BASH_REMATCH[1]}"
        done

        echo 127.0.0.1\ \ \ \ \ \ www.${_DOMAIN} ${_DOMAIN} $_LINE;
        ;;

    ###################################
    ## Catch invalid option
    ###################################
    *)
        if [ -z $1 ]; then
            usage
            exit 0
        else
            errorMsg "Invalid argument: $1"
            exit 1
        fi
    ;;
esac
