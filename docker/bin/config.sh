#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value


#######################################
## Configuration
#######################################

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR="$SCRIPT_DIR/../.."
DOCKER_DIR="$SCRIPT_DIR/.."

PROJECT_DIR="$ROOT_DIR/project"
BACKUP_CURRENT_DIR="$PROJECT_DIR/db"

# Special handling for Apfel
IS_APFEL=0
if [ `uname` == 'Darwin'  ]; then
    IS_APFEL=1
fi

#######################################
## Functions
#######################################

errorMsg() {
    echo "[ERROR] $*"
}

logMsg() {
    echo " * $*"
}

sectionHeader() {
    echo "*** $* ***"
}

execInDir() {
    echo "[RUN :: $1] $2"

    sh -c "cd \"$1\" && $2"
}

dockerContainerId() {
    echo "$(docker-compose ps -q "$1" 2> /dev/null || echo "")"
}

dockerExec() {
    docker exec -i "$(docker-compose ps -q app)" "$@"
}

dockerExecMySQL() {
    docker exec -i "$(docker-compose ps -q mysql)" "$@"
}

dockerCopyFrom() {
    PATH_DOCKER="$1"
    PATH_HOST="$2"
    docker cp "$(docker-compose ps -q app):${PATH_DOCKER}" "${PATH_HOST}"
}
dockerCopyTo() {
    PATH_HOST="$1"
    PATH_DOCKER="$2"
    docker cp "${PATH_HOST}" "$(docker-compose ps -q app):${PATH_DOCKER}"
}
dockip() {
  docker inspect --format '{{ .NetworkSettings.Networks.docker_default.IPAddress }}' "$@"
}