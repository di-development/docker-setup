#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/config.sh"

function usage {
    echo "usage:"
    echo "$0 COMMAND [ARGS]"
    echo
    echo "Command:"
    echo " show                     List all networks"
    echo " add [name] [network]     Add new network"
    echo " remove [name]            Remove existing network"
}

if [ "$#" -lt 1 ]; then
    errorMsg "Missing arguments!"
    usage
    exit 1
fi

# Environement
_NETWORK="docker_network"

# Read in current.conf
if [ -f current.conf ]; then
    source current.conf
fi


case "$1" in
    ###################################
    ## Show help
    ###################################
    "help")
        usage
        ;;

    ###################################
    ## List all vhosts
    ###################################
    "show")
        sectionHeader "Networks:"
        for line in `docker network ls -f type=custom --format "{{.Name}}" 2>/dev/null`
        do
            logMsg ${line}
        done
        ;;

    ###################################
    ## Add new vhost
    ###################################
    "add")
        sectionHeader "Add network"

        if [ "$#" -lt 3 ]; then
            errorMsg "Missing arguments!"
            usage
            exit 1
        fi

        _NETWORK=$2
        _IP=$3

        docker network create --driver=bridge --subnet="${_IP}" "${_NETWORK}" 1> /dev/null
        logMsg "Network ${_NETWORK} ($_IP) created"
        ;;

    ###################################
    ## Remove existing vhost
    ###################################
    "remove")
        sectionHeader "Remove network"
        if [ "$#" -lt 2 ]; then
            usage
            exit 0
        fi

        _NETWORK=$2
        docker network rm "${_NETWORK}" 1> /dev/null
        logMsg "Network ${_NETWORK} removed"
        ;;

    ###################################
    ## Catch invalid option
    ###################################
    *)
        if [ -z $1 ]; then
            usage
            exit 0
        else
            errorMsg "Invalid argument: $1"
            exit 1
        fi
    ;;
esac