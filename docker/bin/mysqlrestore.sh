#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

BIN_DIR="$( dirname "${BASH_SOURCE[0]}" )"
source "$( cd $BIN_DIR && pwd )/config.sh"

# Default mysql credentials
MYSQL_USER=$(dockerExecMySQL printenv MYSQL_USER)
MYSQL_PASSWORD=$(dockerExecMySQL printenv MYSQL_PASSWORD)
MYSQL_DATABASE=$(dockerExecMySQL printenv MYSQL_DATABASE)
MYSQL_ROOT_PASSWORD=$(dockerExecMySQL printenv MYSQL_ROOT_PASSWORD)

# Read current.conf to overwite MYSQL_DATABASE
if [ -f "$BIN_DIR/../current.conf" ]; then
  source "$BIN_DIR/../current.conf"
fi

# Insert dump
if [ -f ../project/db/current.sql ]; then
  docker-compose exec mysql sh -c "MYSQL_PWD='$MYSQL_ROOT_PASSWORD' mysql -u root $MYSQL_DATABASE < /project/db/current.sql"
  logMsg "Database restored"
else
	errorMsg "No dump file at /project/db/current.sql"
	return 1
fi

