#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/config.sh"

#
if [ ! -f "$(pwd)/current.conf" ]; then
	logMsg "No precofigured env!"
	exit 0
fi

# Read in current configuration
source "$(pwd)/current.conf"

unamestr=`uname`

# Reset environment.yml
if [ "$unamestr" == 'Darwin'  ]; then
    sed -i "" -e "s/MYSQL_DATABASE.*/MYSQL_DATABASE=dbname/" "$(pwd)/docker/web/conf/environment.yml"
else
    sed -i -e "s/MYSQL_DATABASE.*/MYSQL_DATABASE=dbname/" "$(pwd)/docker/web/conf/environment.yml"
fi

# Reset env
if [ -f "$(pwd)/.env" ]; then
	if [ "$unamestr" == 'Darwin'  ]; then
	    sed -i "" -e "s/COMPOSE_PROJECT_NAME.*$/COMPOSE_PROJECT_NAME=_PROJECT_/" "$(pwd)/.env"
	else
	    sed -i -e "s/COMPOSE_PROJECT_NAME.*$/COMPOSE_PROJECT_NAME=_PROJECT_/" "$(pwd)/.env"
	fi
fi

# Remove our network
$(pwd)/bin/network.sh remove "${_NETWORK_NAME}" 2>/dev/null || true

rm -f current.conf
rm -f docker/mysql/Dockerfile
rm -f docker/web/Dockerfile
rm -f etc/*/vhost.d/*.conf
rm -f docker-compose.yml