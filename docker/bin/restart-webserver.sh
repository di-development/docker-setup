#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/config.sh"

if [ ! -f "$(pwd)/current.conf" ]; then
    errorMsg "Missing current.conf! Please run 'make envconf'!"
    exit 1
fi

# Read in current configuration
source "$(pwd)/current.conf"

# Container where webserver is running in
_CONTAINER="app";

# Command to restart webserver
_CMD=""
if [ "${_WEBSERVER}" == "nginx" ]; then
    _CMD="/usr/local/bin/service nginx restart"
else
    _CMD="/usr/local/bin/service apache2 restart"
fi

# Run command in docker container 'app'
sectionHeader "Restarting webserver ${_WEBSERVER}"
docker-compose exec ${_CONTAINER} sh -c "${_CMD}";
logMsg "done"
