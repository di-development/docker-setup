#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/config.sh"

function usage {
    echo "usage:"
    echo "$0 COMMAND VHOST"
    echo
    echo "Command:"
    echo " up              Build and start any static container"
    echo " start           Start any static container"
    echo " down            Stop any static container"
    echo " stop            Stop any static container"
    echo " restart         Restart any static container"
    echo " rebuild         Rebuild any static container"
    echo " state           Show state of static containers"
    echo
}

# The main docker dir
MAINDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )
STATICDIR="${MAINDIR}/static/"

# Source the env
source "${STATICDIR}/.env"

case "$1" in
    ###################################
    ## Show help
    ###################################
    "help")
        usage
        ;;

    ###################################
    ## Build and start any static container
    ###################################
    "up")
        cd "$STATICDIR" && docker-compose up -d
        ;;

    ###################################
    ## Start any static container
    ###################################
    "start")
        cd "$STATICDIR" && docker-compose start
        ;;

    ###################################
    ## Stop any static container
    ###################################
    "stop")
        cd "$STATICDIR" && docker-compose stop
        ;;
    "down")
        cd "$STATICDIR" && docker-compose stop
        ;;

    ###################################
    ## Restart any static container
    ###################################
    "restart")
        cd "$STATICDIR" && docker-compose stop && docker-compose up -d
        ;;

    ###################################
    ## Rebuild any static container
    ###################################
    "rebuild")
        cd "$STATICDIR" && docker-compose stop && \
        docker-compose pull && \
        docker-compose rm --force nginx-proxy && \
        docker-compose build --no-cache --pull && \
        docker-compose up -d --force-recreate
        ;;

    ###################################
    ## Show state of static containers
    ###################################
    "state")
        cd "$STATICDIR" && echo $(pwd) &&  docker ps --filter="name=nginx-proxy"
        ;;

    ###################################
    ## Catch invalid option
    ###################################
    *)
        if [ -z $1 ]; then
            usage
            exit 0
        else
            errorMsg "Invalid argument: $1"
            exit 1
        fi
    ;;
esac
