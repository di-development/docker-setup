#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/config.sh"

# Load env for $COMPOSE_PROJECT_NAME
source .env

# stop all dynamic containers
docker-compose stop 2>/dev/null || true

# Remove all dynamic containers 
docker ps -a -q --filter="name=${COMPOSE_PROJECT_NAME}" | xargs docker rm -fv 2>/dev/null || true

# Remove any image
docker images "${COMPOSE_PROJECT_NAME}_*" -q | xargs docker rmi -f 2>/dev/null || true

# Remove volumes from dynamic containers
#docker volume prune -f 2>/dev/null || true
docker volume ls -q --filter="name=${COMPOSE_PROJECT_NAME}" | xargs docker volume rm -f 2>/dev/null || true
