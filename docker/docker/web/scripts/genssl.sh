#!/bin/bash

# Generate SSL Cert
openssl genrsa -passout pass:x -out server.pass.key 4096
openssl rsa -passin pass:x -in server.pass.key -out server.key
rm server.pass.key
openssl req -new -key server.key -out server.csr \
  -subj "/C=DE/ST=Baden-Württemberg/L=Freiburg/O=kultwerk Gmbh/OU=Development/CN=*.docker"
openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
