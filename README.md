Docker Setup - Befehle

1.) $ make envconf development apache mysql project:testproject db:testproject domain:testproject.docker 

2.) $ make up && make staticUp

3.) $ make vhost add myvhost

4.) $ make vhost export

(copy the output into step 5.)

5.) $ sudo nano /etc/host

6.) $ make restartWeb

7.) http://testproject.docker 